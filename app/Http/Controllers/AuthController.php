<?php

namespace App\Http\Controllers;

use App\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//
		$this->graph_api_url = "https://graph.facebook.com/v2.7/";
	}

	/**
	 * Retrieve the user for the given access token from facebook.
	 *
	 * @param  string $access_token
	 *
	 * @return Response
	 */
	public function social_login( Request $request ) {
		$access_token = $request->input( 'access_token' );
		$user_data    = $this->get_facebook_api( $access_token, "me" );

		if ( isset( $user_data["error"] ) ) {
			$output["status"] = 400;
			$output["error"]  = $user_data["error"];
		} elseif ( isset( $user_data["name"] ) ) {
			$output["status"]          = 200;
			$output["data"]["name"]    = $user_data["name"];
			$output["data"]["picture"] = $user_data["picture"]["data"]["url"];
		} else {
			$output["status"]           = 500;
			$output["error"]["message"] = "unknown error";
		}

		return json_encode( $output );
	}

	private function get_facebook_api( $access_token, $path = "me", $fields = "id,name,picture{url}", $format = "json", $method = "get" ) {
		$ch  = curl_init();
		$url = $this->graph_api_url . $path . "?access_token=$access_token&fields=$fields&format=$format&method=$method&pretty=0";
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		$server_output = curl_exec( $ch );

		curl_close( $ch );

		return json_decode( $server_output, true );

	}
}